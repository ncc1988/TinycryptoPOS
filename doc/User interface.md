# How to use tinycryptoPOS

## Login and Dashboard

When you access the tinycryptoPOS start page you will be prompted for a username and a password, if you're not already logged in. After that you will be redirected to the dashboard where you can see all important information about your received payments.

### Verified Balance

Below the field "(verified) Balance" you can see what amount of any cryptocurrency you accept via tinycryptoPOS is verified by the corresponding cryptocurrency network. By now, verified means that there are at least 6 confirmations of a transaction in the cryptocurrency network.

### Incomplete Payments

If you have incomplete payments (payments that are either not paid or not verified) you will see a list with all details of that payment. By clicking on one item in the list you can see the QR code with the cryptocurrency URI so that a customer may have a second chance to pay his bill.

The capital letter "P" means that the customer paid his bill. If you see a little "p" it means that no payment has been received yet. Please make sure that your cryptocurrency client is fully synchronized with the network, because unsynchronised clients may not show the most recent payments. If your client is fully synchronised then you may be victim of fraud.

The capital letter "V" means the cryptocurrency network has confirmed the payment's transaction at least 6 times and thus is considered valid. The little "v" means the payment is confirmed 5 times or less. If you have a payment that is not verified after some time (Bitcoin: 1 hour, Litecoin: 15 minutes) you should check if your cryptocurrency client is fully synchronised with the network. If it is and the payment is not verified then you may be a victim of fraud.

In case you don't have any incomplete payment you will see the text "No incomplete payments".

### Actions

If you click on "New payment" you will be redirected to the payment processing page where you can enter and accept a new payment.

"Logout" will log you out of tinycryptoPOS.

## Accept a new payment

First you need to select a cryptocurrency. If no cryptocurrency is activated tinycryptoPOS will ask you to go to the configuration page and add a cryptocurrency. Because the configuration page is not working at the moment (2015-11-05) you need to use the Django admin application as described in the installation document (installation.md) to add a cryptocurrency.

After you selected a cryptocurrency you need to enter the amount you want to charge to your customer. Be aware that the number field is not working properly at the moment (2015-11-05) and thus you need to enter the amount with a keyboard or a keyboard simulator on your touchscreen device.

After you have entered an amount greater zero and clicked on "Make Payment" you will see a QR-Code that should be shown to your customer so that he can scan it with its mobile device.

After your customer has paid you a smiling emoticon on the screen with the text "Payment successful!" above it should appear a few seconds later. If this isn't the case you should check your internet connection.

When you click on "Return to Dashboard" you can accept a new payment. If you click on "Return to Dashboard" directly after you were paid by your customer you probably will see a new incomplete payment. This is normal since it takes some time until a transaction is confirmed at least 6 times.
