# Installation manual of tinycryptoPOS

## Requirements

You need the following software requirements to run tinycryptoPOS:

* Python 2.7 or compatible
* Django (tinycryptoPOS is tested with Django version 1.7)
* At least one bitcoind compatible cryptocurrency client with compatible JSON-RPC calls. The client has to be synchronised with the cryptocurrency network.
* A XHTML compliant web browser with CSS3 and JavaScript support

Hardware requirements are as follows:

* Desktop screen (Landscape orientation): Minimum height 768 px
* Mobile screen (Portrait orientation): Minimum width 320 px

## Installation

### Django

First you need to setup a new Django project in a desired directory with the following command:
    django-admin startproject your_project_name

### Source code

After that you go into the new directory your_project_name and clone the tinycrpyptoPOS repository inside of it:
    git clone https://gitlab.com/ncc1988/TinycryptoPOS.git

### Django project settings

Now you need to add tinycryptoPOS to the list of installed applications of your Django project. For this you open the file settings.py in the your_project_name subdirectory and add the string 'tinycryptoPOS' to the INSTALLED_APPS tuple.

In the your_project_name subdirectory you will also find the file urls.py where you have to include tinycryptoPOS's URLs:
* first add a line to import tinycryptoPOS's urls.py file:
    from tinycryptoPOS.urls import urls as tcPOS_urls
* after that you add an URL object to the urlpatterns tuple:
    url(r'^tinycryptoPOS/', include(tcPOS_urls)),

### Database

Now you need to setup the database with the following command:
    python manage.py syncdb

## Configuration

At the moment you need the Django built-in admin application to configure tinycryptoPOS. After login to the admin application you need to do the following steps to start using tinycryptoPOS.

### Add currency

Go to the "Currencys" form. You will find this in the "Tinycryptopos" box. There you klick on "Add currency" and enter the official name and short name of the currency. Be aware that the name you enter will be used (in lowercase letters) for URIs in QR-Codes. So if you enter "Bitcoin" as name, the URI will start with "bitcoin:".
After entering a name and a short name you should enable the currency by checking "Enabled". Don't forget to save the currency because it is needed in the next step.

### Add currency daemon

Currency daemon entries tell tinycryptoPOS how to connect to a bitcoind compatible client and its JSON-RPC interface. You need to add a currency daemon to actually use a currency.

After you clicked "Add currency daemon" you need to select the right currency from the dropdown menu, add the host IP or hostname (without http:// or https://), the port on which the cryptocurrency client's JSON-RPC interface is reachable, and the username and password for authentication. At the moment the password field is not shadowed in the admin interface. After you click on "Save" you have finished the necessary steps to setup tinycryptoPOS.

