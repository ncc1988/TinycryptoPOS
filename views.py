#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import io
import json
from decimal import *

from django.shortcuts import render
from django.http import HttpResponse, Http404, HttpResponseForbidden, HttpResponseRedirect, HttpResponseBadRequest
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.middleware.csrf import CsrfViewMiddleware
from django.core.urlresolvers import reverse

from TinycryptoPOS.models import Currency, CurrencyDaemon, Payment, OnlineShopConfig
from TinycryptoPOS.Classes import CurrencyDaemonException, CryptoCurrencyException
from TinycryptoPOS.forms.NewCurrencyDaemonForm import NewCurrencyForm, NewCurrencyDaemonForm
from TinycryptoPOS.forms.online_shop_forms import NewOnlineShopForm
from TinycryptoPOS.forms.delete_forms import DeleteConfirmationForm
from TinycryptoPOS.plugins.ExchangeRates.DummyExchange import DummyExchange
from TinycryptoPOS.plugins.online_shops.Dummy import Dummy as DummyShop
from TinycryptoPOS.plugins.online_shops.Magento import Magento as MagentoShop
from TinycryptoPOS.plugins.online_shops.online_shop_plugin import ProductNotFoundException


class VerifiedBalance():
  def __init__(self):
    self.balance = 0.0
    self.currency_short_name = ""
  def __str__(self):
    return '{:f}'.format(self.balance) + ' ' + self.currency_short_name

class ExchangeRate():
  def __init__(self, source_currency=None, target_currency=None, rate=None):
    if(not isinstance(source_currency, Currency)):
      raise Exception("ExchangeRate: source_currency must be a Currency instance!")
    if(not isinstance(target_currency, Currency)):
      raise Exception("ExchangeRate: target_currency must be a Currency instance!")
    if(not isinstance(rate, float)):
      raise Exception("ExchangeRate: rate must be of type float!")
    
    self.source_currency = source_currency
    self.target_currency = target_currency
    self.rate = rate


# Create your views here.

def dashboard(request):
    if (request.user.is_authenticated()):
        error_messages = []
        verified_balances = []
        incomplete_payments = []
        daemons = CurrencyDaemon.objects.all()
        for cd in daemons:
            try:
                vb = VerifiedBalance()
                vb.balance = float(cd.get_verified_balance() / 100000000.0)
                vb.currency_short_name = cd.currency.short_name
                #print (str(vb))
                verified_balances.append(vb)
                incomplete_payments += cd.get_incomplete_payments()
            except CurrencyDaemonException as e:
                error_messages.append('Error: '+str(e))
        return render(request, 'TinycryptoPOS/Dashboard.html',
                    {
                        'user':request.user,
                        'verified_balances':verified_balances,
                        'incomplete_payments':incomplete_payments,
                        'msgList': error_messages
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def payment_currency_select(request):
    if (request.user.is_authenticated()):
        crypto_currencies = Currency.objects.filter(is_cryptocurrency=True, enabled=True)
        #if(len(crypto_currencies) == 1):
            ##only one cryptocurrency: redirect directly to the payment method select view:
            #return HttpResponseRedirect(reverse('TinycryptoPOS-payment_method_select', crypto_currencies[0].id))
        #else:
        return render(request, 'TinycryptoPOS/PaymentCurrencySelect.html',
                    {
                        'crypto_currencies':crypto_currencies,
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def payment_method_select(request, currency_id):
    if (request.user.is_authenticated()):
        try:
            crypto_currency = Currency.objects.get(pk=currency_id)
        except Currency.DoesNotExist:
            raise Http404("Currency does not exist!")
        if(crypto_currency.enabled == False):
            return HttpResponseForbidden("Selected currency is not enabled!")
        if(crypto_currency.is_cryptocurrency == False):
            return HttpResponseForbidden("Selected currency is not a cryptocurrency!")
        non_crypto_currency = Currency.objects.get(short_name='EUR')
        return render(request, 'TinycryptoPOS/PaymentMethodSelect.html', {'crypto_currency_id': crypto_currency.id})
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def simple_payment_view(request, currency_id):
    #currency_id is the ID of a cryptocurrency
    if (request.user.is_authenticated()):
        crypto_currency = Currency.objects.get(pk=currency_id)
        if(crypto_currency.enabled == False):
            return HttpResponseForbidden("Selected currency is not enabled!")
        if(crypto_currency.is_cryptocurrency == False):
            return HttpResponseForbidden("Selected currency is not a cryptocurrency!")
        non_crypto_currency = Currency.objects.get(short_name='EUR')
        exchange_rate = None
        try:
            exc = DummyExchange() #TODO: make exchange rate provider configurable
            rate = exc.get_exchange_rate(crypto_currency, non_crypto_currency)
            exchange_rate = ExchangeRate(crypto_currency,non_crypto_currency,rate)
        except Exception as e:
            print("Exception with source="+str(crypto_currency)+" and target="+str(non_crypto_currency)+": "+str(e))
        
        return render(request, 'TinycryptoPOS/SimplePaymentView.html',
                    {
                        'crypto_currency':crypto_currency,
                        'non_crypto_currency':non_crypto_currency,
                        'exchange_rate':exchange_rate
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def online_shop_select_view(request, currency_id):
    if (request.user.is_authenticated()):
        try:
            cur = Currency.objects.get(pk=currency_id)
        except Currency.DoesNotExist:
            raise Http404("Currency does not exist!")
        if(cur.enabled == False):
            return HttpResponseForbidden("Selected currency is not enabled!")
        if(cur.is_cryptocurrency == False):
            return HttpResponseForbidden("Selected currency is not a cryptocurrency!")
        
        online_shops = OnlineShopConfig.objects.all()
        return render(request, 'TinycryptoPOS/OnlineShopSelectView.html',
                    {
                        'crypto_currency_id': cur.id,
                        'online_shops':online_shops
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def online_shop_payment_view(request, currency_id, online_shop_id):
    if (request.user.is_authenticated()):
        try:
            crypto_currency = Currency.objects.get(pk=currency_id)
        except Currency.DoesNotExist:
            raise Http404("Currency does not exist!")
        try:
            online_shop_config = OnlineShopConfig.objects.get(pk=online_shop_id)
        except OnlineShopConfig.DoesNotExist:
            raise Http404("Chosen online shop system does not exist!")
        
        non_crypto_currency = Currency.objects.get(short_name='EUR')
        exchange_rate = None
        try:
            exc = DummyExchange()
            exchange_rate = exc.get_exchange_rate(crypto_currency, non_crypto_currency)
        except Exception as e:
            print("Exception with source="+str(crypto_currency)+" and target="+str(non_crypto_currency)+": "+str(e))
        
        shop = None
        if(online_shop_config.plugin == 'Dummy'):
            shop = DummyShop(online_shop_config.url, online_shop_config.username, online_shop_config.password)
        elif(online_shop_config.plugin == 'Magento'):
            shop = MagentoShop(online_shop_config.url, online_shop_config.username, online_shop_config.password)
        else:
            raise Http404('{"error":"Unknown online shop system. Check your installation!", "class":"shop"}')
        
        return render(request, 'TinycryptoPOS/OnlineShopPaymentView.html',
                    {
                        'crypto_currency':crypto_currency,
                        'non_crypto_currency':non_crypto_currency,
                        'online_shop_config':online_shop_config,
                        'exchange_rate':exchange_rate,
                        'online_shop_has_order_support': shop.has_order_support(),
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def online_shop_payment_get_product(request, currency_id, online_shop_id):
    #this is requested via AJAX
    if (request.user.is_authenticated()):
        try:
            currency = Currency.objects.get(pk=currency_id)
        except Currency.DoesNotExist:
            raise Http404('{"error":"Currency not found!", "class":"currency"}')
        try:
            ean = int(request.GET['ean'])
        except Exception:
            return HttpResponseBadRequest('{"error":"Malformed request!", "class":"request"}')
        try:
            online_shop_config = OnlineShopConfig.objects.get(pk=online_shop_id)
        except CMS.DoesNotExist:
            return HttpResponse(content='{"error":"Online shop not found!", "class":"shop"}', status=404) #TODO: make this a JSON response
        
        shop = None
        if(online_shop_config.plugin == 'Dummy'):
            shop = DummyShop(online_shop_config.url, online_shop_config.username, online_shop_config.password)
        elif(online_shop_config.plugin == 'Magento'):
            shop = MagentoShop(online_shop_config.url, online_shop_config.username, online_shop_config.password)
        else:
            raise Http404('{"error":"Unknown online shop system. Check your installation!", "class":"shop"}')
        try:
            product = shop.get_product_information_by_ean(ean)
        except ProductNotFoundException:
            return HttpResponse(content='{"error":"Product not found!", "class":"product"}', status=404)
        return HttpResponse(product.to_json())
    else:
        return HttpResponseForbidden()


def create_new_payment(request):
    if (request.user.is_authenticated()):
        if(CsrfViewMiddleware().process_view(request, None, None, None) == None):
            if('PayWithOnlineShop' in request.POST):
                """
                The user wants to pay via the online shop system
                so we must create a shopping cart in the online shop system
                and redirect the user to the online shop system and the cart
                """
                online_shop_config = OnlineShopConfig.objects.get(pk=request.POST['online_shop_id'])
                products = request.POST['products[]'].lstrip().split(' ')
                print("Product list: " + str(products))
                shop = None
                if(online_shop_config.plugin == 'Dummy'):
                    shop = DummyShop(online_shop_config.url, online_shop_config.username, online_shop_config.password)
                elif(online_shop_config.plugin == 'Magento'):
                    shop = MagentoShop(online_shop_config.url, online_shop_config.username, online_shop_config.password)
                else:
                    raise Exception("Unknown CMS system! Check your Installation!")
                payment_url = shop.create_payment(products)
                return HttpResponseRedirect(payment_url)
            else:
                try:
                    """
                    The user wants to pay directly via TinycryptoPOS:
                    we have to direct him to the payment page
                    after the payment has been registered.
                    """
                    amount = float(request.POST['amount'])
                    currency_name = request.POST['currency']
                except Exception:
                    raise Exception("Malformed request!")
                currency = Currency.objects.filter(name=currency_name)
                #print ("CreateNewPayment: Amount="+str(amount))
                if(len(currency) == 1):
                    if(amount > 0):
                        #exactly one currency matches the name and the amount is higher than zero
                        currency = currency[0]
                        daemon = currency.daemon
                        amount = int(amount * 100000000) #convert to satoshis
                        payment = daemon.create_payment(user=request.user, amount=amount)
                        #display the payment page:
                        return render(request, 'TinycryptoPOS/PayView.html',
                                    {
                                        'payment':payment
                                    })
                    else:
                        #zero amount
                        raise Exception("Amount is zero!")
                else:
                    raise Http404("Currency not found!")
        else:
            return HttpResponseForbidden("CSRF token verification failed!")
    else:
        return HttpResponseForbidden()
  

def update_payment_status(request, payment_id):
    #this is handled via AJAX
    if (request.user.is_authenticated()):
        try:
            payment = Payment.objects.get(pk=payment_id)
            daemon = payment.currency.daemon
            daemon.update_payment_status(payment)
            return HttpResponse('{"paymentID":'+str(payment.id)+', "paid":'+str(payment.paid).lower()+', "verified":'+str(payment.verified).lower()+'}')
        except Payment.DoesNotExist:
            raise Http404
    else:
        return HttpResponseForbidden()


def show_qr_code(request, payment_id):
    if (request.user.is_authenticated()):
        try:
            payment = Payment.objects.get(pk=payment_id)
            qr_data = io.BytesIO()
            payment.generate_qr_code_image().save(qr_data, "PNG")
            return HttpResponse(qr_data.getvalue(), content_type="image/png")
        except Payment.DoesNotExist:
            raise Http404
    else:
        return HttpResponseForbidden()


def config_dashboard(request):
    if(request.user.is_authenticated()):
        try:
            crypto_currencies = Currency.objects.filter(is_cryptocurrency=True)
        except Currency.DoesNotExist:
            crypto_currencies = []
        try:
            crypto_currency_daemons = CurrencyDaemon.objects.all()
        except CurrencyDaemon.DoesNotExist:
            crypto_currency_daemons = []
        try:
            online_shop_configs = OnlineShopConfig.objects.all()
        except OnlineShopConfig.DoesNotExist:
            online_shop_configs = []
        
        return render(request, 'TinycryptoPOS/ConfigDashboard.html',
                    {
                        'crypto_currencies':crypto_currencies,
                        'crypto_currency_daemons':crypto_currency_daemons,
                        'online_shop_configs':online_shop_configs
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))


def config(request, action=None, object_type=None, object_id = None):
    if(action == None):
        return HttpResponseBadRequest('Invalid request!')
    if(object_type == None):
        return HttpResponseBadRequest('Invalid request!')
    
    if (request.user.is_authenticated()):
        displayed_form = None
        message_list = []
        if(action == 'add'):
            #the user wants something added:
            if(object_type == 'crypto_currency'):
                if (request.method == 'POST'):
                    new_currency_form = NewCurrencyForm(request.POST, prefix='new_currency')
                    if(new_currency_form.is_valid()):
                        new_currency_form.save()
                        message_list.append('New crypto currency was saved!')
                    else:
                        displayed_form = {
                                'title':'Add or change crypto currency',
                                'submit_title':'Save currency',
                                'form':new_currency_form(prefix='new_currency')
                            }
                        message_list.append('There are one or more errors in the form!')
                else:
                    displayed_form = {
                            'title':'Add or change crypto currency',
                            'submit_title':'Save currency',
                            'form':NewCurrencyForm(prefix='new_currency')
                        }
            
            elif(object_type == 'currency_daemon'):
                if (request.method == 'POST'):
                    new_currency_daemon_form = NewCurrencyDaemonForm(request.POST, prefix='new_currency_daemon')
                    if(new_currency_daemon_form.is_valid()):
                        new_currency_daemon_form.save()
                        message_list.append('New Currency Daemon registered!')
                    else:
                        displayed_form = {
                                'title':'Add or change crypto currency daemon',
                                'submit_title':'Save daemon configuration',
                                'form':new_currency_daemon_form(prefix='new_currency_daemon')
                            }
                        message_list.append('There are one or more errors in the form!')
                else:
                    displayed_form = {
                            'title':'Add or change crypto currency daemon',
                            'submit_title':'Save daemon configuration',
                            'form':NewCurrencyDaemonForm(prefix='new_currency_daemon')
                        }
            
            elif(object_type == 'online_shop_config'):
                if (request.method == 'POST'):
                    new_online_shop_config_form = NewOnlineShopForm(request.POST, prefix='new_online_shop_config')
                    if(new_online_shop_config_form.is_valid()):
                        new_online_shop_config_form.save()
                        message_list.append('New online shop configuration was saved!')
                    else:
                        message_list.append('There are one or more errors in the form!')
                        displayed_form = {
                                'title':'Add a configuration for an online shop system',
                                'submit_title':'Save online shop configuration',
                                'form':new_online_shop_config_form(prefix='new_online_shop_config')
                            }
                else:
                    displayed_form = {
                            'title':'Add a configuration for an online shop system',
                            'submit_title':'Save online shop configuration',
                            'form':NewOnlineShopForm(prefix='new_online_shop_config')
                        }
        elif(action == 'delete'):
            if(object_id == None):
                return HttpResponseBadRequest('Invalid request!')
            message_list = []
            
            if(request.method == 'POST'):
                #the delete action was confirmed
                success = False
                confirmation_form = DeleteConfirmationForm(request.POST)
                if(confirmation_form.is_valid()):
                    #we continue only when the form matches the request URL!
                    if((confirmation_form.cleaned_data['object_type'] == object_type) and
                       (int(confirmation_form.cleaned_data['object_id']) == int(object_id))):
                        if(object_type == 'crypto_currency'):
                            try:
                                currency = Currency.objects.get(pk=object_id)
                                currency.delete()
                                message_list.append('Currency was deleted!')
                                success = True
                            except Currency.DoesNotExist:
                                message_list.append('Error: Currency does not exist!')
                        if(object_type == 'currency_daemon'):
                            try:
                                currency_daemon = CurrencyDaemon.objects.get(pk=object_id)
                                currency_daemon.delete()
                                message_list.append('Currency daemon was deleted!')
                                success = True
                            except CurrencyDaemon.DoesNotExist:
                                message_list.append('Error: Currency daemon does not exist!')
                        if(object_type == 'online_shop_config'):
                            try:
                                online_shop_config = OnlineShopConfig.objects.get(pk=object_id)
                                online_shop_config.delete()
                                message_list.append('Online shop configuration was deleted!')
                                success = True
                            except OnlineShopConfig.DoesNotExist:
                                message_list.append('Error: Online shop configuration does not exist!')
                        return render(request, 'TinycryptoPOS/Config.html',
                            {
                                'form': None,
                                'message_list':message_list,
                            })
                    else:
                        return HttpResponseBadRequest("Invalid request: Form parameters don't match request URL!")
                else:
                    return HttpResponseBadRequest('Invalid request: Form invalid!')
            else:
                #TODO: load object from database to display the object that is going to be deleted
                displayed_form = {
                        'title':'Do you really want to delete the selected item?',
                        'submit_title':'Really delete',
                        'form':DeleteConfirmationForm({'object_type':object_type, 'object_id':object_id})
                    }
                
        return render(request, 'TinycryptoPOS/Config.html',
                    {
                        'message_list':message_list,
                        'form':displayed_form,
                    })
    else:
        return HttpResponseRedirect(reverse('TinycryptoPOS-login'))
