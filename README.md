# Readme / FAQ

## What is TinycryptoPOS?

TinycryptoPOS is a lightweight web-based Point-Of-Sale system for cryptocurrencies like Bitcoin, Litecoin and others. It can be seen as a middleman between the cryptocurrency client and the web browser on the merchant's computer.

## Why was this created?

This was created during my master thesis for the course applied computer science at Environmental Campus Birkenfeld. With this project I want to answer the question how easy it is to build a point-of-sale system for cryptocurrencies.

## Where can I find installation instructions?

See the file ./doc/Installation.md for installation instructions.

## How to use TinycryptoPOS?

The documentation for the user interface is placed in the file ./doc/User interface.md.

## How is TinycryptoPOS licensed?

TinycryptoPOS is licensed under the terms of the GNU Affero General Public License Version 3. Please bear in mind that this means that the software is provided 'as is', so there is NO WARRANTY at all.

## How can I contribute?

You may contribute by testing the software and report bugs. TinycryptoPOS can be considered alpha quality software by now (2015-11-05) and thus the design is far away from being perfect and some functionality may not work properly.

## Are you accepting donations?

Yes. The easiest way would be to send some Bitcoins, Litecoins or Peercoins to the following addresses:

* Bitcoin: 19rBB7dXcg5eXCKnveRbtes9sbURRLpYgP
* Litecoin: LTd5qhQ8yjQ3zf7dtq3XVXrrjKckakj5i7
* Peercoin: PNFdA9pW4y4Az9tZZRepiCD4PPsYj1GBTT

You can verify the addresses with my OpenPGP public key (ID 38D5 F800 0777 1EB9) and the signed message from the following URL:
https://mstrohm.sirius.uberspace.de/pgp-dateien/Spendenadressen.txt.asc
