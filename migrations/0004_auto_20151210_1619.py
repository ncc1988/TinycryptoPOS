# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TinycryptoPOS', '0003_cmsconfig_url'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cmsconfig',
            old_name='cmsPlugin',
            new_name='cms_plugin',
        ),
        migrations.RenameField(
            model_name='currency',
            old_name='isCryptocurrency',
            new_name='is_cryptocurrency',
        ),
        migrations.RenameField(
            model_name='payment',
            old_name='addedBy',
            new_name='added_by',
        ),
    ]
