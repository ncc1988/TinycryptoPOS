# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=24)),
                ('shortName', models.CharField(max_length=8)),
                ('enabled', models.BooleanField(default=False)),
                ('isCryptocurrency', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CurrencyDaemon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('host', models.CharField(max_length=24)),
                ('port', models.PositiveIntegerField()),
                ('username', models.CharField(max_length=128)),
                ('password', models.CharField(max_length=256)),
                ('currency', models.OneToOneField(related_name='daemon', to='TinycryptoPOS.Currency')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.CharField(max_length=128)),
                ('amount', models.PositiveIntegerField()),
                ('label', models.CharField(max_length=24, null=True)),
                ('paid', models.BooleanField(default=False)),
                ('verified', models.BooleanField(default=False)),
                ('addedBy', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('currency', models.ForeignKey(to='TinycryptoPOS.Currency')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
