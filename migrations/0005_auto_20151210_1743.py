# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TinycryptoPOS', '0004_auto_20151210_1619'),
    ]

    operations = [
        migrations.RenameField(
            model_name='currency',
            old_name='shortName',
            new_name='short_name',
        ),
    ]
