# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('TinycryptoPOS', '0002_cmsconfig'),
    ]

    operations = [
        migrations.AddField(
            model_name='cmsconfig',
            name='url',
            field=models.URLField(default='http://example.org'),
            preserve_default=False,
        ),
    ]
