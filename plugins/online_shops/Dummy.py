#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import decimal

from TinycryptoPOS.Classes import Product
from django.core.urlresolvers import reverse

class Dummy():
    def __init__(self, api_url=None, api_user=None, api_password=None):
        self.api_url = api_url
        self.api_user = api_user
        self.api_password = api_password
    
    
    def get_product_information_by_ean(self, product_ean = None):
        #retrieve product information by the product's EAN code
        if(product_ean == None):
            raise Exception("get_product_information_by_ean: Error: product_ean is None (or not set)!")
        p = Product(name="EAN"+str(product_ean), ean=product_ean, price=1337, currency="EUR", id=int(product_ean))
        return p
    
    def create_payment(self, product_list=None):
        if(product_list):
            return reverse('TinycryptoPOS-dashboard')
    
    def has_order_support(self):
        #if the online shop can handle orders that are sent from TinycryptoPOS this returns True.
        #Otherwise: False
        return False