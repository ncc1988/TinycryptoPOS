#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

class ProductNotFoundException(Exception):
    def __init__(self, value):
        self.value = value
    
    def __str__(self):
        return repr(self.value)


class OnlineShopPlugin():
    def __init__(self, api_url=None, api_user=None, api_password=None):
        self.api_url = api_url
        self.api_user = api_user
        self.api_password = api_password
  
    
    def get_product_information_by_ean(self, product_ean=None):
        """
        Retrieves product information by the product's EAN code.
        The price returned is in cents.
        """
        raise Exception("Function not overloaded!")
  
    def create_payment(self, product_list=None):
        """
        Pay inside the CMS system instead of TinycryptoPOS,
        if this is supported by the CMS system.
        """
        raise Exception("Function not overloaded!")
        
    def has_order_support(self):
        """
        If the online shop can handle orders that are sent from TinycryptoPOS this returns True.
        Otherwise: False
        """
        return False;
