#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import magento
from decimal import Decimal

from TinycryptoPOS.Classes import Product as Product
from online_shop_plugin import ProductNotFoundException


class Magento():
    def __init__(self, api_url=None, api_user=None, api_password=None):
        self.api_url = api_url
        self.api_user = api_user
        self.api_password = api_password
  
  
    def get_product_information_by_ean(self, product_ean = None):
        #retrieve product information by the product's EAN code
        #price is returned in cents
        if(product_ean == None):
            raise Exception("get_product_information_by_ean: Error: product_ean is None (or not set)!")
        with magento.catalog.Product(self.api_url, self.api_user, self.api_password) as product_api:
            product_list = product_api.list({'ean':{'equal':str(product_ean)}})
            if(len(product_list) < 1):
                raise ProductNotFoundException("get_product_information_by_ean: Error: product not found!")
            product = product_list[0]
            details = product_api.info(product['product_id'], attributes=['name', 'ean', 'price'])
            with magento.ProductImages(self.api_url, self.api_user, self.api_password) as image_api:
                product_image_list = image_api.list(product['product_id'])
                if(len(product_image_list) > 0):
                    productImageUrl = image_api.list(product['product_id'])[0]['url'] #we get a list of dictionaries and we're only interested in the url property of the first element
                else:
                    productImageUrl = None
                p = Product(id=product['product_id'], name=unicode(details['name']), ean=int(details['ean']), price=Decimal(details['price']), currency="EUR", image_url=productImageUrl) #TODO: make currency configurable
                return p
  
    def create_payment(self, product_list = None):
        raise Exception("Not supported! Magento cannot do this anymore (with Magento versions > 1.7)")
        #if(not isinstance(productList, list)):
          #raise Exception("create_payment: productList is not a list!")
        #with magento.Cart(self.api_url, self.api_user, self.api_password) as cartApi:
            
    def has_order_support(self):
        return False;
    