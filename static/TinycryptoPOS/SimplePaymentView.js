/*
   This file is part of TinycryptoPOS
   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function CalculateExchanged(element)
{
  //console.log("CalculateExchanged");
  if(element == undefined)
  {
    return false;
  }
  
  var amount = parseFloat(element.value);
  var exchangeRate = parseFloat(element.getAttribute("data-exchangeRate"));
  
  var exchanged = Math.round((amount / exchangeRate)*100000000.0)/100000000.0;
  
  document.getElementById("ExchangedCurrencyValue").innerHTML = exchanged;
  document.getElementById("CryptoCurrencyAmount").value = exchanged;
}


function AddNumber(number)
{
  if(number != undefined)
  {
    amountField = document.getElementById("PaymentAmountField");
    //for text input:
    if(amountField.value.length < 1)
    {
      amountField.value = number;
    }
    else
    {
      if(amountField.value == '.')
      {
        //there is just a decimal separator but no number in fromt of it: change it to zero, the separator and the number:
        amountField.value = '0.' + number;
      }
      else if (amountField.value == '0')
      {
        //if there is a zero we can replace it with the new number:
        amountField.value = number;
      }
      else
      {
        amountField.value += number;
      }
    }
    amountField.onchange();
  }
}

function AddSeparator()
{
  amountField = document.getElementById("PaymentAmountField");
  /*sep = 1.1
  sep = sep.toLocaleString().substring(1, 2);
  console.log("Separator = ["+sep+"]");
  console.log("Value = "+amountField.value.toString() + '.0');
  amountField.value = parseFloat(amountField.value.toString() + '.0');
  */
  //workaround:
  if(amountField.value.indexOf('.') === -1)
  {
    //only add separator when there is no separator yet
    amountField.value += '.';
  }
  //amountField.value += ',';
  amountField.onchange();
}

function DeleteNumber()
{
  amountField = document.getElementById("PaymentAmountField");
  amountField.value = amountField.value.substring(0, amountField.value.length - 1);
  amountField.onchange();
}


