/*
   This file is part of TinycryptoPOS
   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var productList = []; //global variable


function ConvertPrice(nonCCPrice)
{
  var exchangeRate = parseFloat(document.getElementById("ExchangeRate").value);
  return (Math.round((parseFloat(nonCCPrice) / exchangeRate) * 100000000.0) / 100000000.0);
}


function CalculateTotalPrice()
{
    //add price to total:
    //FIXME: total cryptocurrency price may differ from sum of single prices (difference of 1 satoshi observed)
    var nonCCTotal = 0.0;
    
    for(var i = 0; i < productList.length; i++)
    {
        nonCCTotal += productList[i].price;
    }
    
    var total = ConvertPrice(nonCCTotal);
    
    document.getElementById("Total").innerHTML = total.toFixed(8);
    document.getElementById("TotalNonCC").innerHTML = nonCCTotal.toFixed(2);
    
    //set hidden total input field in payment form:
    document.getElementById("FormTotalField").value = total.toFixed(8);
}


function RemoveProduct(productListId)
{
    if(productListId == undefined)
    {
        return false;
    }
    
    //first retrieve the product from the product ID list:
    productPosition = -1;
    for(var i = 0; i < productList.length; i++)
    {
        if(productList[i].list_id == productListId)
        {
            productPosition = i;
        }
    }
    if(productPosition == -1)
    {
        //product not found!
        return false;
    }
    
    product = productList[productPosition];
    productList.splice(productPosition,1);
    
    table = product.tableRow.parentNode;
    table.removeChild(product.tableRow);
    
    CalculateTotalPrice();
}


function AddProduct(product)
{
  if(product == undefined)
  {
    return false;
  }
  
  //convert string to float:
  product.price = parseFloat(product.price);
  
  //add Product to item list:
  
  //first generate the HTML elements:
  var table = document.getElementById("ItemList");
  var tr = document.createElement("tr");
  table.appendChild(tr);
  var tdName = document.createElement("td");
  tr.appendChild(tdName);
  var tdPrice = document.createElement("td");
  tr.appendChild(tdPrice);
  var tdAction = document.createElement("td");
  tr.appendChild(tdAction);
  
  var convertedPrice = ConvertPrice(product.price).toFixed(8);
  
  //then fill them with data:
  tdName.innerHTML = product.name;
  tdPrice.innerHTML = convertedPrice + ' ' + document.getElementById("Currency").innerHTML;
  tdAction.innerHTML = 'X';
  tdAction.classList.add('Action');
  tdAction.classList.add('RedAction');
  
  //update product info page:
  var productNameField = document.getElementById("ProductName");
  if(product.name.length > 27)
  {
    productNameField.innerHTML = product.name.substring(0,27)+'...';
  }
  else
  {
    productNameField.innerHTML = product.name;
  }
  
  document.getElementById("ProductPriceCC").innerHTML = ConvertPrice(product.price).toFixed(8);
  document.getElementById("ProductCurrencyCC").innerHTML = document.getElementById("Currency").innerHTML;
  document.getElementById("ProductPriceNonCC").innerHTML = product.price.toFixed(2);
  document.getElementById("ProductCurrencyNonCC").innerHTML = document.getElementById("CurrencyNonCC").innerHTML;
  
  
  //scroll product table to bottom:
  table.scrollTop = table.scrollHeight;
  
  
  //show product image (if available):
  if(product.image_url != null)
  {
    document.getElementById("ProductImage").setAttribute("src", product.image_url);
  }
  
  //append product (and some additional attributes) to productList:
  product.list_id = productList.length;
  product.tableRow = tr;
  
  productList.push(product);
  //we can't use the position directly. This would fail
  //if the user deletes more than one element at a random list position.
  tdAction.onclick = function(){
      RemoveProduct(product.list_id);
  }
  
  CalculateTotalPrice();
  
  //append product id to product ID field:
  document.getElementById("FormProductIdField").value += " "+product.id;
  
  //no errors: no error message (and delete the old one, if one is there):
  document.getElementById("ErrorMessageField").innerHTML = "";
  
  //enable ean input field:
  var eanField = document.getElementById("ProductEan");
  eanField.value = ""; //clear the field's value
  eanField.disabled = false;
  eanField.focus();
}


function GetProductPrice(ean)
{
  if(ean == undefined)
  {
    return false;
  }
  var request = new XMLHttpRequest();
  request.open("GET", './GetProduct/?ean='+ean, true);
  request.onreadystatechange = function()
    {
      if(request.readyState == 4)
      {
        switch(request.status)
        {
            case 200:
            {
                product = JSON.parse(String(request.responseText));
                AddProduct(product);
                break;
            }
            case 404:
            {
                error = JSON.parse(String(request.responseText));
                errorField = document.getElementById("ErrorMessageField");
                errorField.innerHTML = error.error;
                
                //enable ean input field:
                var eanField = document.getElementById("ProductEan");
                eanField.value = ""; //clear the field's value
                eanField.disabled = false;
                eanField.focus();
                break;
            }
            case 500:
            {
                errorField = document.getElementById("ErrorMessageField");
                errorField.innerHTML = "Internal server error! Please try again or call your system administrator!";
                
                //enable ean input field:
                var eanField = document.getElementById("ProductEan");
                eanField.value = ""; //clear the field's value
                eanField.disabled = false;
                eanField.focus();
            }
        }
      }
    };
  
  request.send();
}


function AddScannedProduct(sender)
{
  if(sender == undefined)
  {
    return false;
  }
  
  sender.disabled = true;
  
  //clear the error field:
  errorField = document.getElementById("ErrorMessageField");
  errorField.innerHTML = "&nbsp;";
  
  //now ask for the product EAN:
  
  GetProductPrice(sender.value);
}


function ValidatePayment()
{
  var form = document.forms[0];
  if(form.elements['amount'].value == 0)
  {
    document.getElementById("ErrorMessageField").innerHTML = "Error: Sum is zero! Please add at least one product!";
    //give the EAN field the focus:
    document.getElementById("ProductEan").focus();
    return false;
  }
  
  form.submit();
}