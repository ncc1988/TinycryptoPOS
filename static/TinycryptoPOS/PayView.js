/*
   This file is part of TinycryptoPOS
   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

updateTimer = null
updatePayment = null


function UpdatePaymentStatus()
{
  if(updatePayment == undefined)
  {
    return false;
  }
  var request = new XMLHttpRequest();
  request.open("GET", '../ajax/paymentStatus/'+updatePayment+'/', true);
  request.onreadystatechange = function()
    {
      if((request.readyState == 4) && (request.status == 200))
      {
        payment = JSON.parse(String(request.responseText));
        if(payment.paid)
        {
          document.getElementById("PaymentRequestDialog").style.display="none";
          document.getElementById("PaidDialog").style.display="block";
          updatePayment = undefined;
          window.clearInterval(updateTimer);
        }
      }
    };
  request.send();
}


window.onload = function()
{
  updatePayment = document.getElementById("PaymentID").value;
  updateTimer = window.setInterval(UpdatePaymentStatus, 5000); //check the payment status every 5 seconds
};
