#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import django.forms as forms
from TinycryptoPOS.models import OnlineShopConfig


class NewOnlineShopForm(forms.ModelForm):
    #plugin = forms.ChoiceField(widget=forms.Select(choices=(('0','Dummy',), ('1','Magento',)), initial='Magento'))
    
    class Meta:
        model = OnlineShopConfig
        fields = ['name', 'plugin', 'url', 'username', 'password']
        
        