#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.forms import ModelForm
from TinycryptoPOS.models import CurrencyDaemon, Currency

class NewCurrencyForm(ModelForm):
  class Meta:
    model = Currency
    fields = ['name', 'short_name', 'enabled']

class NewCurrencyDaemonForm(ModelForm):
  class Meta:
    model = CurrencyDaemon
    fields = ['currency', 'host', 'port', 'username', 'password']
  #currency = forms.ModelChoiceField(queryset=None)
  #host = forms.CharField(max_length=24)
  #port = forms.PositiveIntegerField()
  #username = forms.CharField(max_length=128)
  #password = forms.PasswordField(max_length=256)
  #
  #def __init__(self, *args, **kwargs):
  #  super(NewCurrencyDaemonForm, self).__init__(*args, **kwargs)
  #  self.fields['currency'].queryset = Currency.objects.all()
