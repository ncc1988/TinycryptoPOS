#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django import forms
from TinycryptoPOS.models import Payment, CMSConfig

#this is not used!
class NewPaymentForm(forms.Form):
  amount = forms.PositiveIntegerField(required=True, initial=0, widget=forms.HiddenInput())
  currencyId = forms.PositiveIntegerField(required=True, initial=0, widget=forms.HiddenInput())
  payWithCMS = forms.BooleanField(initial=False, widget=forms.HiddenInput())
  cmsConfigId = forms.PositiveIntegerField(required=False, initial=0, widget=forms.HiddenInput())
  products = forms.CharField(required=False, widget=forms.HiddenInput())
  
  
  