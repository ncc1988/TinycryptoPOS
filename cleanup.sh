#!/bin/sh

rm -r ./forms/*.pyc 2>/dev/null
rm -r ./migrations/*.pyc 2>/dev/null
rm -r ./*.pyc 2>/dev/null
rm -r ./templatetags/*.pyc 2>/dev/null
rm -r ./plugins/*.pyc 2>/dev/null
rm -r ./plugins/ExchangeRates/*.pyc 2>/dev/null
rm -r ./plugins/online_shops/*.pyc 2>/dev/null
echo "DONE!"
