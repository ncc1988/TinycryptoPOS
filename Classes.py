#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json
import requests
from decimal import Decimal

#all classes that are used internally (and are not stored in the database) are placed here (or will be moved here)

class CurrencyDaemonException(Exception):
  #this is raised when there is a problem with the currency daemon
  def __init__(self, value):
    self.value = value
  
  def __str__(self):
    return repr(self.value)


class CryptoCurrencyException(Exception):
  #this is raised when the is an error related to the cryptocurrency: not enough money, bad address, ...
  def __init__(self, value):
    self.value = value
  
  def __str__(self):
    return repr(self.value)
  

class JSONRPCClient():
  #pyjsonrpc was incompatible with Django on the amd64 architecture (see debian bug 804340)
  #so I have written a simple JSON-RPC client myself.
  def __init__(self, url=None):
    if(not isinstance(url, basestring)):
      raise Exception("JSONRPCClient: url must be a string!")
    #if(not isinstance(username, basestring)):
    #  raise Exception("JSONRPCClient: username must be a string!")
    #if(not isinstance(password, basestring)):
    #  raise Exception("JSONRPCClient: password must be a string!")
    
    #turl = url.split("://") #split at protocol separator
    #self.url=turl[0]+"://"+username+":"+password+"@"+turl[1]
    #print("UNSAFE DEBUG: self.url="+str(self.url))
    self.url=url
    #self.username=username
    #self.password=password
  
  def request(self, function=None, params=None):
    if(not isinstance(function, basestring)):
      raise Exception("JSONRPCClient: request: function must be a string!")
    if(not isinstance(params, list)):
      raise Exception("JSONRPCClient: request: params must be a list!")
    
    header = {
      "method":function,
      "params":params,
      "jsonrpc":"2.0",
      "id":0
      }
    
    response = requests.post(self.url, data=json.dumps(header), headers=header).json()
    #print("reponse="+str(response))
    #check if the response is what we want:
    #assert response["jsonrpc"] #only json-rpc 2.0: it has to be a jsonrpc object
    assert response["id"] == 0
    #assert response["method"] == function #only json-rpc 2.0
    return response["result"]


class Product():
  def __init__(self, id=None, name=None, ean=None, price=None, currency = None, image_url=None):
    self.id = None
    self.name = None
    self.ean = None
    self.price = None
    self.currency = None
    self.image_url = None
    
    if(id != None):
      self.id = id;
    if(name != None):
      self.name = unicode(name)
    if(ean != None):
      self.ean = int(ean)
    if(price != None):
      self.price = Decimal(price) #convert the price to the smalles unit: cents in some fiat currencies or satoshis in cryptocurrencies
    if(currency != None):
      self.currency = str(currency) #should include the 3-letter currency code (or more letters for cryptocurrencies like DOGE)
    if(image_url != None):
      self.image_url = image_url #image_url: product image URL
  
  def to_json(self):
    d = dict()
    d['id'] = self.id
    d['name'] = self.name
    d['ean'] = self.ean
    #convert price from cents to normal value:
    d['price'] = '{:.2f}'.format(self.price)
    d['currency'] = self.currency
    d['image_url'] = self.image_url
    return json.dumps(d)
