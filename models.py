#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import qrcode

from requests import ConnectionError

from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User

from TinycryptoPOS.Classes import JSONRPCClient, CurrencyDaemonException, CryptoCurrencyException


class Currency(models.Model):
    name = models.CharField(max_length=24, unique=True)
    short_name = models.CharField(max_length=8) #short name, e.g. XBT, BTC, LTC, DOGE, PPC, ...
    enabled = models.BooleanField(default=False)
    is_cryptocurrency = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.name


class Payment(models.Model):
    added_by = models.ForeignKey(User)
    currency = models.ForeignKey(Currency)
    address = models.CharField(max_length=128, null=False) #maybe some cryptocurrency will implement longer addresses, thus 128 chars allowed
    amount = models.PositiveIntegerField(null=False) #amount in satoshis
    label = models.CharField(max_length=24, null=True, blank=True)
    paid = models.BooleanField(default=False) #true only after the full amount was paid
    verified = models.BooleanField(default=False) #true only after 6 confirmations
  
    def __unicode__(self):
        s = str(float(self.amount) / 100000000.0) + ' ' +self.currency.short_name + ' to ' + self.address + ' ('
        if(self.paid):
            s += 'P'
        else:
            s += 'p'
        if(self.verified):
            s += 'V'
        else:
            s += 'v'
        s += '), added by '+str(self.added_by)
        return s
  
    def generate_qr_code(self):
        #build BIP 0021 compliant bitcoin URI: https://en.bitcoin.it/wiki/BIP_0021
        code = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_M, box_size=10, border=4)
        uri = self.currency.name.lower()+':'+unicode(self.address)+'?amount='+str(float(self.amount)/100000000)
        #disabled until URL encoding is implemented:
        #if(isinstance(self.label, basestring)):
        # uri += '?label='+str(self.label) #TODO: URL-encode!
        code.add_data(uri)
        code.make(fit=True)
        return code
  
    def generate_qr_code_image(self):
        code = self.generate_qr_code()
        return code.make_image()



class CurrencyDaemon(models.Model):
    currency = models.OneToOneField(Currency, related_name="daemon")
    host = models.CharField(max_length=24)
    port = models.PositiveIntegerField()
    username = models.CharField(max_length=128)
    password = models.CharField(max_length=256)
    
    def __unicode__(self):
        return self.currency.name+'@'+self.host+':'+str(self.port)
    
    def get_server(self):
        return JSONRPCClient('http://'+str(self.username)+':'+str(self.password) + '@' + str(self.host) + ':' + str(self.port))
    
    def create_payment(self, user=None, amount=None, label=None):
        if(not isinstance(user, User)):
            raise Exception("user must be a Django User instance!")
        if(not isinstance(amount, int)):
            raise CryptoCurrencyException("amount must be specified in satoshis!")
        server = self.get_server()
        newPayment = Payment()
        newPayment.added_by = user
        newPayment.currency = self.currency
        try:
            newPayment.address = server.request("getnewaddress", [])
        except ConnectionError as e:
            raise CurrencyDaemonException("Cryptocurrency daemon "+str(self)+" not responding!")
        newPayment.amount = amount
        newPayment.label = label
        newPayment.save()
        print("DEBUG: New payment registered!")
        return newPayment
    
    def get_received_amount(self, payment):
        if(not isinstance(payment, Payment)):
            raise Exception("get_received_amount: Error: payment needs to be a Payment instance!")
        server = self.get_server()
        try:
            amount = server.request("getreceivedbyaddress", [payment.address, 0]) # minconf=0
        except ConnectionError as e:
            raise CurrencyDaemonException("Cryptocurrency daemon "+str(self.currencyDaemon)+" not responding!")
        amount = int(amount * 100000000)
        return amount
    
    def payment_is_confirmed(self, payment):
        if(not isinstance(payment, Payment)):
            raise Exception("payment_is_confirmed: Error: payment needs to be a Payment instance!")
        server = self.get_server()
        minConfirmation = 6 # to be configurable
        try:
            confirmedAmount = int(float(server.request("getreceivedbyaddress", [payment.address, minConfirmation])) * 100000000.0)
        except ConnectionError as e:
            raise CurrencyDaemonException("Cryptocurrency daemon "+str(self)+" not responding!")
        #all transactions to this address must be confirmed at least 6 times by the network:
        if(confirmedAmount >= payment.amount): #greater or equal: leave the opportunity for tips
            return True
        else:
            return False
    
    def update_payment_status(self, payment):
        if(not isinstance(payment, Payment)):
            raise Exception("update_payment_status: Error: payment must be a Payment instance!")
        amount = self.get_received_amount(payment)
        if(amount >= payment.amount):
            payment.paid = True
            payment.save()
        if(self.payment_is_confirmed(payment)):
            payment.verified = True
            payment.save()
    
    def get_verified_balance(self):
        server = self.get_server()
        try:
            accounts = server.request("listaccounts", [6]) #6 minimum confirmations
        except ConnectionError as e:
            raise CurrencyDaemonException("Cryptocurrency daemon "+str(self)+" not responding!")
        balance = 0
        for a in list(accounts.viewvalues()):
            balance += int(a * 100000000)
        return balance
    
    def get_incomplete_payments(self):
        incompletePayments = Payment.objects.filter(Q(paid=False) | Q(verified=False))
        for i in incompletePayments:
            #update the payment status first to check if the payment was completed after the last check:
            self.update_payment_status(i)
        
        #now filter again to get the list of still incomplete payments:
        return Payment.objects.filter(Q(paid=False) | Q(verified=False))
    


class OnlineShopConfig(models.Model):
    name = models.CharField(max_length=96, unique=True)
    plugin = models.CharField(max_length=48) #the name of the online shop plugin #TODO: check if safe (probably only with extra checks!)
    url = models.URLField()
    username = models.CharField(max_length=128) #CMS API user name (or API key)
    password = models.CharField(max_length=128) #CMS API password (or API secret)