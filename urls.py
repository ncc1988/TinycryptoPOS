#
#   This file is part of TinycryptoPOS
#   Copyright (C) 2015-2016 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import patterns, include, url

urls = patterns('',
    url(r'^$', 'TinycryptoPOS.views.dashboard', name='TinycryptoPOS-dashboard'),
    url(r'^payment/(?P<currency_id>\d+)/online_shop/(?P<online_shop_id>\d+)/GetProduct/$', 'TinycryptoPOS.views.online_shop_payment_get_product', name='TinycryptoPOS-online_shop_payment_get_product'),
    url(r'^payment/(?P<currency_id>\d+)/online_shop/(?P<online_shop_id>\d+)/$', 'TinycryptoPOS.views.online_shop_payment_view', name='TinycryptoPOS-online_shop_payment_view'),
    url(r'^payment/(\d+)/online_shop', 'TinycryptoPOS.views.online_shop_select_view', name='TinycryptoPOS-online_shop_select_view'),
    url(r'^payment/(\d+)/simple$', 'TinycryptoPOS.views.simple_payment_view', name='TinycryptoPOS-simple_payment_view'),
    url(r'^payment/(\d+)/$', 'TinycryptoPOS.views.payment_method_select', name='TinycryptoPOS-payment_method_select'),
    url(r'^payment/$', 'TinycryptoPOS.views.payment_currency_select', name='TinycryptoPOS-payment_currency_select'),
    url(r'^newPayment/$', 'TinycryptoPOS.views.create_new_payment', name='TinycryptoPOS-create_new_payment'),
    url(r'^config/$', 'TinycryptoPOS.views.config_dashboard', name='TinycryptoPOS-config_dashboard'),
    url(r'^config/(?P<action>[a-z_]+)/(?P<object_type>[a-z_]+)$', 'TinycryptoPOS.views.config', name='TinycryptoPOS-config'),
    url(r'^config/(?P<action>[a-z_]+)/(?P<object_type>[a-z_]+)/(?P<object_id>[0-9]+)$', 'TinycryptoPOS.views.config', name='TinycryptoPOS-config'),
    url(r'^login', 'django.contrib.auth.views.login', {'template_name': 'TinycryptoPOS/Login.html'}, name='TinycryptoPOS-login'),
    url(r'^logout', 'django.contrib.auth.views.logout', {'template_name': 'TinycryptoPOS/Logout.html'}, name='TinycryptoPOS-logout'),
    url(r'^payment/qr/(\d+)/$', 'TinycryptoPOS.views.show_qr_code', name='TinycryptoPOS-show_qr_code'),
    url(r'^ajax/paymentStatus/(\d+)/$', 'TinycryptoPOS.views.update_payment_status', name='TinycryptoPOS-update_payment_status'),
    
    )
